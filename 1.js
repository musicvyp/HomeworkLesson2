/*

  Задание 1.

  Написать скрипт который будет будет переключать вкладки по нажатию
  на кнопки в хедере.

  Главное условие - изменять файл HTML нельзя.

  Алгоритм:
    1. Выбрать каждую кнопку в шапке
       + бонус выбрать одним селектором

    2. Повесить кнопку онклик
        button1.onclick = function(event) {

        }
        + бонус: один обработчик на все три кнопки

    3. Написать функцию которая выбирает соответствующую вкладку
       и добавляет к ней класс active

    4. Написать функцию hideAllTabs которая прячет все вкладки.
       Удаляя класс active со всех вкладок

  Методы для работы:

    getElementById
    querySelector
    classList
    classList.add
    forEach
    onclick

    element.onclick = function(event) {
      // do stuff ...
    }

*/
var button = document.getElementsByClassName("showButton");
var page = document.getElementsByClassName("tab");

console.log(button);

button[0].id = "cat1";
button[1].id = "cat2";
button[2].id = "cat3";

var arrButton = [cat1,cat2,cat3];
console.log(arrButton);

page[0].id = "first";
page[1].id = "second";
page[2].id = "third";

var arrPage = [first, second, third];
console.log(arrPage);

arrButton.forEach(function(event) {

    event.onclick = function(item){
      if (event.getAttribute("data-tab") == 1) {
      first.classList.toggle('active');
    }
      else if (event.getAttribute("data-tab") == 2) {
        second.classList.toggle('active');
    }
      else if (event.getAttribute("data-tab") == 3) {
        third.classList.toggle('active');
    }
  }
})
function hideAllTabs(item) {
    arrPage.forEach(function(item){
    item.classList.remove('active');
  })
}

console.log(setInterval(hideAllTabs, 5000));

//          Начальный вариант назначения функции кнопкам

  // button[0].onclick = function(event) {

//     page[0].classList.toggle('active');

// }

// button[1].onclick = function(event) {

//     page[1].classList.toggle('active');

// }
// button[2].onclick = function(event) {

//     page[2].classList.toggle('active');
// }

